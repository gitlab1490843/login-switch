document.addEventListener('DOMContentLoaded', function () {
    const switches = document.querySelectorAll('.switch input');
    switches.forEach((checkbox, index) => {
      checkbox.addEventListener('change', function () {
        switches.forEach((otherCheckbox, otherIndex) => {
          if (otherIndex !== index) {
            otherCheckbox.checked = false;
          }
        });
      });
    });
  });