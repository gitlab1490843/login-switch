document.addEventListener('DOMContentLoaded', function () {
    setupDropdown('dropdownInput', 'dropdownContent');
    setupDropdown('dropdownInput2', 'dropdownContent2');
    setupDropdown('field7', 'dropdownContent3');
    setupDropdown('field8Dropdown', 'dropdownContent5');
    setupDropdown('field10Dropdown', 'dropdownContent6');
});
function setupDropdown(inputId, contentId) {
    var inputField = document.getElementById(inputId);
    var dropdownContent = document.getElementById(contentId);
    inputField.addEventListener('click', function () {
        dropdownContent.style.display = (dropdownContent.style.display === 'block') ? 'none' : 'block';
    });
    document.addEventListener('click', function (event) {
        if (!event.target.closest('.input-with-arrow')) {
            dropdownContent.style.display = 'none';
        }
    });
    dropdownContent.addEventListener('click', function (event) {
        if (event.target.tagName === 'DIV') {
            inputField.value = event.target.innerText;
            dropdownContent.style.display = 'none';
        }
    });
}
